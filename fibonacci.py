#
#   COMPLETE THIS TEMPLATE AND STORE AS "fibonacci.py"
#
# essentially, you need to fill the spaces marked with "YOUR CODE HERE"
#





#------------------------------------------------------------------------------
# compute the n-th Fibonacci number fn recursively
#
# input:
#    n : index of Fib. number to be computed
# output:
#   fn : n-th Fib. number
#------------------------------------------------------------------------------
def fibonacci_rec(n):
    # safety check
    if n < 0:
        print('Error: Input must be a positive number!')
        return 0
    elif n == 0:
        fn = 0
    elif n == 1:
        fn = 1
    else:
        fn = fibonacci_rec(n-1) + fibonacci_rec(n-2)

    return fn
#---End-fibonacci_rec----------------------------------------------------------      



#------------------------------------------------------------------------------
# compute the n-th Fibonacci number fn iteratively
#
# input:
#    n : index of Fib. number to be computed
# output:
#   fn : n-th Fib. number
#------------------------------------------------------------------------------
def fibonacci_iter(n):
    # initialize starting condition
    fn_prev = 1
    fn_prev_prev = 0
    fn = 0
    # safety check
    if n < 0:
        print('Error: Input must be a positive number')
    elif n == 0:
        # special initial case n=0
        fn = 0
    elif n == 1:
        # special initial case n=0
        fn = 1
    else:
        # YOUR CODE HERE
        while n > 1:
            fn = fn_prev + fn_prev_prev
            fn_prev_prev = fn_prev
            fn_prev = fn
            n -= 1
    return fn
#--End_fibonacci_iter----------------------------------------------------------




do_test = 0
if do_test:
    for k in range(10):
        print(fibonacci_rec(k))
    for k in range(10):
        print(fibonacci_iter(k))
