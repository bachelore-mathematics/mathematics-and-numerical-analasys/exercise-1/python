#------------------------------------------------------------------------------
# script for timing the computational effort of the 
# recursive and iterative Fibonaaci number algorithm
#
# WORKS ONLY, WHEN YOU SUPPLY THE FUNCTIONS 
#   fibonacci_rec  and   fibonacci_iter
# contained in the file fibonacci.py
#------------------------------------------------------------------------------


# import the required modules
import numpy as np
import timeit

# for plotting function graphs
import matplotlib
import matplotlib.pyplot as plt


#local module: this contains the functions "fibonacci_rec" and "fibonacci_iter"
import fibonacci as fib

# 
#
# which Fib. number is to be computed?
input_n = input('Enter the index of the Fib. number to be computed: ')
# the input is a text string, i.e., input 3 is not the number 3
# but the text string '3' as if on a keyboard
# need to covert this to an integer number
n = int(input_n)

print("Computing Fibonacci number", n)


# Python's timeit function measures cpu time

t_start = timeit.default_timer()
fn = fib.fibonacci_rec(n)
t_end = timeit.default_timer()
print('fn = ', fn)
print('recursive computation finished in ', t_end-t_start, 's')
print(' ')


t_start = timeit.default_timer()
fn = fib.fibonacci_iter(n)
t_end = timeit.default_timer()
print('fn = ', fn)
print('iterative computation finished in ', t_end-t_start, 's')
print(' ')


# compute the ratio rn = fn+1/fn for n = 1,...,40
n = 40

#create a numpy array filled with zeros
# syntax: np.zeros((n,m) ) creates a matrix of dimensions nxm
#         np.zeros((n,) )  creates a vector of dimensions n
fibs_ratios = np.zeros((n,) )
for k in range(1,n+1):
    fibs_ratios[k-1] = fib.fibonacci_iter(k+1)/fib.fibonacci_iter(k)

golden = 0.5*(1 + np.sqrt(5))*np.ones((n,))
errors  = abs(fibs_ratios - golden)

print('The ratio of two consecutive Fib. numbers compared to the golden ratio:')



# plot the error over the Fib. number on a logarithmic scale.
line_error = plt.plot(range(1,n+1), errors,  'k-', linewidth=2, label = 'error')
plt.yscale('log')  # log-scale for y-axis
plt.legend()
plt.xlabel('n')
plt.ylabel('error (fn+1)/fn - golden ratio')
plt.title('Fib ratio compared to golden ratio:')
plt.show()



