#
#   COMPLETE THIS TEMPLATE AND STORE AS "babylon_sqrt.py"
#
# essentially, you need to fill the spaces marked with "YOUR CODE HERE"
#



#------------------------------------------------------------------------------
# Babylonian methofd for computing square roots
#
# Input:
#    a  : number > 0
#    x0 : starting point (arbitrary, but >0)
#
# Output:
#  sqrt_a : square root of a
#  n_iter : number of iterations until convergence was detected 
#------------------------------------------------------------------------------
def  babylon_sqrt(a, x0):
    # Check for proper starting criteria
    if  x0 < 1:
        return False

    #
    # starting point for iteration sequence
    x = x0

    # this is to avoid infinite loops:
    max_iter = 1000

    # count the iteration number
    nr_iter = 0

    #initialize
    fx = max_iter


    while abs(fx-x)>1.0e-10 and nr_iter < max_iter:
        # compute next iterate
        if  x == 1:
            x = 0.5*(a/x0 + x0 )
        elif x > 1 :
            x = 0.5*(a / (x) + (x))
        else:
            return False
        fx = 0.5*(a / x + x )
        # update iteration counter
        nr_iter = nr_iter + 1
    # end of while-loop    
    sqrt_a = fx

    return sqrt_a, nr_iter
