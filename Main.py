# Exercise
# use one, consist of four problem where problem 3 and 4 is python specific problems and problem 1 and 2 is not.
# There will be created a file for each "problem", named after the topic.
# So for problem 3, where the topic is fibonacci number
# the py file will be name fibonacci. Each problem will be a function of its own.
# In the main function a "test"/problem solving code is placed. This is used for solving the problem/exercise and test
# the functions.

# Import moduels
import timeit
import matplotlib.pyplot as plt

# Import the different problem files.
import fibonacci as fib
import babylon_sqrt as bas


# Problem 1 - Fibonacci ratio
def prob1():
    print("Problem 1")
    print("Exercise 1.B")
    int_fibonacci = int(input("Type in the number of fibonaccu numbers to check for convergens and fix point: "))
    y = [0] * int_fibonacci
    x = range(int_fibonacci)

    for i in range(2,int_fibonacci+2):
        y[i-2] = fib.fibonacci_iter(i) / fib.fibonacci_iter(i-1)

    float_conv = y[int_fibonacci -1 ] 
    float_fixp = 1 + 1/float_conv 

    int_precission = 6

    if round(float_conv,int_precission) == round(float_fixp,int_precission):
        print(str(float_conv) + " is a fix point") 
    else:
       print(str(float_conv) + " is not a fix point") 
       print(float_fixp)
    plot = plt.plot(x,y)
    plt.show()

# Problem 2 - Babylonian Square root
def  prob2():
    print("Problem 2")


# Problem 3 - Computing Fibonacci python programming
def prob3():
    print("Problem 3")
    print("Type skip, to skip the sub exercise")
    print("Recursive function - Exercise 3.A")
    int_fib_number = input("Select a fib number : ")
    if int_fib_number != "skip":
        print(fib.fibonacci_rec(int(int_fib_number)))
    print("Iterative function - Exercise 3.B")
    int_fib_number = input("Select a fib number : ")
    if int_fib_number != "skip":
        print(fib.fibonacci_iter(int(int_fib_number)))
    print("Compare computation power used for recursive and iterative method - Exercise 3.B")

    n = 10

    for i in range(3):

        t_start = timeit.default_timer()
        fn = fib.fibonacci_rec(n)
        t_end = timeit.default_timer()
        print('fn = ', fn)
        print('recursive computation finished in ', t_end - t_start, 's')
        print(' ')

        t_start = timeit.default_timer()
        fn = fib.fibonacci_iter(n)
        t_end = timeit.default_timer()
        print('fn = ', fn)
        print('iterative computation finished in ', t_end - t_start, 's')
        print(' ')
        n = n * 2


# Problem 4 - Babylonian square root python programming
def prob4():
     tuple_result = (2,5)
     print("Problem 4")
     a = int(input("Type in the number of which the square root is to be calculated : "))
     x0 = int(input("Type in the starting criteria for the calculation : "))
     tuple_result = bas.babylon_sqrt(a,x0)
     print("Calculating the square root of " + str(a) + ", and the result is : " + str(tuple_result[0]) + " and it took " + str(tuple_result[1]) + " of iteration")



# Press the green buttow
# in the gutter to run the script.
if __name__ == '__main__':
    problem = "First"
    while problem != "exit":
        problem = input("Choose problem to run: ")
        if problem == "1":
            prob1()
        elif problem == "2":
            prob2()
        elif problem == "3":
            prob3()
        elif problem == "4":
            prob4()
        elif problem == "exit":
            print("Closing the Exercise")
        else:
            print("Select a problem from 1-4")
